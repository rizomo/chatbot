# Makefile to build and push Docker images

# Targets for building images
build-rasa-actions:
	docker build -t registry.mim-libre.fr/rizomo/chatbot/rasa-actions -f ./Dockerfile.rasa-actions --platform linux/amd64 .

build-back:
	docker build -t registry.mim-libre.fr/rizomo/chatbot/back -f ./Dockerfile.back --platform linux/amd64 .

build-front:
	docker build -t registry.mim-libre.fr/rizomo/chatbot/front -f ./Dockerfile.front --platform linux/amd64 .

# Targets for pushing images
push-rasa-actions:
	docker push registry.mim-libre.fr/rizomo/chatbot/rasa-actions

push-back:
	docker push registry.mim-libre.fr/rizomo/chatbot/back

push-front:
	docker push registry.mim-libre.fr/rizomo/chatbot/front

# Combined build and push targets
deploy-rasa-actions: build-rasa-actions push-rasa-actions

deploy-back: build-back push-back

deploy-front: build-front push-front

# Target to build all images
build-all: build-rasa-actions build-back build-front

# Target to push all images
push-all: push-rasa-actions push-back push-front

# Target to build and push all images
deploy-all: build-all push-all

# Target to build and push all images
all: deploy
