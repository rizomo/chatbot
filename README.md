# Chatbot

## Installation

Clone the repository using `git clone`

Setup env variables
```bash
cp .env.example .env
```
Fill the variables values.

## On first deploy
Run the setup script to install needed operators and helm charts
```bash
bash deploy/kubernetes/setup.sh
```

## Deploy

```bash
kubectl apply -k .
```
